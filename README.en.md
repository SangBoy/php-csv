# php-csv

#### Introduce
Used for processing the CSV data in PHP to import and export operation!


#### Install the tutorial

`composer require sang-boy/php-csv`

#### Directions for use

1.Import data:

```
$csv = Csv::import($request->file('csv')->getRealPath());
try {
    $res = $csv->execute(function ($data) {
        DB::insert(...);
    });
} catch (StreamEmptyException $e) {
    return '';
}
```

2. Export data:

```
Csv::export(['名称','性别'],[['张三','男']])->save(storage_path('csvs'));//Export to the server.
Csv::export(['名称','性别'],[['张三','男']])->download();//Export to the browser.
```
