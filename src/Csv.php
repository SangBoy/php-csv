<?php

namespace SangBoy\PhpCsv;

class Csv
{
    /**
     *
     * @param string|null $stream
     * @param int $startLine
     * @return Import
     */
    public static function import(string $stream = null,int $startLine = 0): Import
    {
        return new Import($stream,$startLine);
    }

    /**
     * @param array $headers
     * @param array $data
     * @param string|null $file_name
     * @return Export
     */
    public static function export(array $headers, array $data, ?string $file_name = null): Export
    {
        return new Export($headers, $data, $file_name);
    }
}
