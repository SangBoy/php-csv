<?php

namespace SangBoy\PhpCsv;

use Closure;
use SangBoy\PhpCsv\Exceptions\StreamEmptyException;

class Import
{
    use CsvConfig;

    /**
     * @var string
     */
    protected $stream;

    /**
     * 跳过空行
     *
     * @var bool $skipEmptyLines
     */
    protected $skipEmptyLines =  false;

    /**
     * 对每个数据执行trim
     *
     * @var bool $trimFields
     */
    protected $trimFields =  true;

    /**
     * 严格模式，开启后，列格式不匹配则直接报错
     *
     * @var bool $strict
     */
    protected $strict =  false;

    /**
     * 起始行
     *
     * @var int $startLine
     */
    protected $startLine = 0;

    /**
     * @var array $headers
     */
    protected $headers = [];

    /**
     * @param string|null $stream
     * @param int $startLine
     */
    public function __construct(string $stream = null,int $startLine = 0)
    {
        $this->stream = $stream;
        $this->startLine = $startLine;
    }

    /**
     * @return int
     */
    public function getStartLine(): int
    {
        return $this->startLine;
    }

    /**
     * @param int $startLine
     * @return $this
     */
    public function setStartLine(int $startLine): Import
    {
        $this->startLine = $startLine;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param Closure|null $closure
     * @return array
     * @throws StreamEmptyException
     */
    public function execute(Closure $closure = null): array
    {
        if(!$this->stream || !trim($this->stream)) {
            Throw new StreamEmptyException();
        }
        $str_arr = [];
        if(file_exists($this->stream)) {
            $handle = fopen($this->stream, 'r');
            while (($line = fgets($handle)) !== false) {
                $str_arr[] = $line;
            }
            fclose($handle);

        } else {
            $str_arr = explode("\r", $this->stream);
        }

        //跳转至初始行
        for ($idx = 0; $idx < $this->getStartLine(); $idx++) {
            unset($str_arr[$idx]);
        }

        $data = [];
        $buffer = '';
        $i = 0;
        foreach ($str_arr as $str) {
            $buffer .= $str;
            $item = $this->parseCsv($buffer);
            if($item !== false){
                $buffer = '';
                $trim_characters = " \t\n\r\0\x0B" . ($this->enclosure ? "\{$this->enclosure}" : '');
                if($i == 0) {//跳过标题列
                    $i++;
                    $this->headers = array_map(function ($v)use($trim_characters){
                        return trim($v,$trim_characters);
                    }, $item);
                    continue;
                }
                if(empty($item)) {//跳过空行
                    $i++;
                    continue;
                }
                $item = array_map(function ($v)use($trim_characters){
                    return trim($v,$trim_characters);
                }, $item);

                $closure && $closure->call($this, $item, $i);
                $data[] = $item;
                $i++;
            }
        }

        return $data;
    }

    /**
     * 解析CSV字符串
     * @param $csv_string
     * @return array|false
     */
    private function parseCsv($csv_string)
    {
        //  如果CSV字符串为空或仅包含分隔符/特殊字符，则返回空数组
        if (!trim($csv_string) || ($this->skipEmptyLines && !trim(str_replace($this->separator, '', $csv_string)))) {
            return [];
        }
        //  如果CSV字符串不包含分隔符，则单独处理
        if (strpos($csv_string, $this->separator) === false) {
            return [$this->trimFields ? trim($csv_string) : $csv_string];
        }
        //  如果数值包裹符号为空，则返回分隔符切割的数组
        if (!$this->enclosure) {
            return explode($this->separator, $csv_string);
        }
        //  定义结果数组
        $res = [];
        while (strlen(trim($csv_string)) > 0) {
            //  如果第一个字符为双引号，则表示要查找双引号包含的复杂文本
            if (substr($csv_string, 0, 1) == $this->enclosure) {
                //  定义双引号结束位置，并设置偏移量为1
                $pos_enclosure_offset = 1;
                while (true) {
                    //  如果在剩余文本中未找到双引号结束符，则表示剩余文本不符合CSV格式，停止查找，并返回结果
                    if (($pos_enclosure = strpos($csv_string, $this->enclosure, $pos_enclosure_offset)) === false) {
                        if ($this->strict) return false;
                        //  将剩余文本添加到结果数组中，trimFields参数控制是否删除文本前后空格
                        $v = substr($csv_string, 1);
                        $res[] = $this->trimFields ? trim($v) : $v;
                        return $res;
                    }
                    //  获取下一个字符，并根据不同的字符类型更新偏移量
                    $next_bt = substr($csv_string, $pos_enclosure + 1, 1);
                    if ($next_bt == '"') {
                        $pos_enclosure_offset = $pos_enclosure + 2;
                        continue;
                    }
                    if ($next_bt && !in_array($next_bt, ["\r", "\n", $this->separator])) {
                        $pos_enclosure_offset = $pos_enclosure + 1;
                        continue;
                    }
                    //  处理转义字符
                    $v = str_replace($this->enclosure.$this->enclosure, $this->enclosure, substr($csv_string, 1, $pos_enclosure - 1));
                    //  将结果添加到数组中，并更新CSV字符串
                    $res[] = $this->trimFields ? trim($v) : $v;
                    $csv_string = substr($csv_string, $pos_enclosure + 2);
                    break;
                }
            } else if (($pos_separator = strpos($csv_string, $this->separator)) === false) {
                //  将剩余文本添加到结果数组中，trimFields参数控制是否删除文本前后空格，同时去除文本前后双引号
                $res[] = $this->trimFields ? trim(trim($csv_string, $this->enclosure)) : trim($csv_string, $this->enclosure);
                return $res;
            } else {
                //  处理当前字段，并更新CSV字符串
                $v = substr($csv_string, 0, $pos_separator);
                $res[] = $this->trimFields ? trim($v) : $v;
                $csv_string = substr($csv_string, $pos_separator + 1);
            }
        }
        //  返回结果数组
        return $res;
    }
}
