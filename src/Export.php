<?php

namespace SangBoy\PhpCsv;

use Closure;

class Export
{
    use CsvConfig;

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var string|null
     */
    protected $file_name;

    /**
     * @var Closure|null $eachCallback
     */
    protected $eachCallback;

    /**
     * @param array $headers
     * @param array $data
     * @param string|null $file_name
     */
    public function __construct(array $headers, array $data, ?string $file_name = null)
    {
        $this->headers = $headers;
        $this->data = $data;
        $this->file_name = $file_name;
    }

    /**
     * @param Closure|null $eachCallback
     */
    public function each(?Closure $eachCallback): void
    {
        $this->eachCallback = $eachCallback;
    }

    /**
     * @return string
     */
    public function getFileName(): ?string
    {
        return $this->file_name ?: date('YmdHims').'-'.$this->randomStr().'.csv';
    }

    /**
     * 生成文件内容
     *
     * @return string
     */
    private function buildContent(): string
    {
        $content = $this->implodeRow(array_values($this->headers));

        $keys = array_keys($this->headers);
        $keys = is_numeric($keys[0] ?? 0) ? [] : $keys;
        foreach ($this->data as $datum) {
            if(!$keys) {
                $content .= $this->implodeRow($datum);
            } else {
                if($this->eachCallback) {
                    $content .= $this->implodeRow($this->eachCallback->call($datum, $datum));
                } else {
                    $content .= $this->implodeRow(array_intersect_key((array)$datum, array_flip($keys)));
                }
            }
        }
        return $content;
    }

    /**
     * 连接成行
     *
     * @param array $item
     * @return string
     */
    private function implodeRow(array $item): string
    {
        return implode($this->separator, array_map(function ($str) {
                return $this->enclosure ? $this->enclosure . $str . $this->enclosure : $str;
            }, $item)) . "\r\n";
    }

    /**
     * 保存至CSV文件中
     *
     * @param $store_path
     * @param string|null $file_name
     * @return bool
     */
    public function save($store_path, ?string $file_name = null): bool
    {
        if (!is_dir($store_path)) {
            mkdir($store_path);
        }

        $path = $store_path . DIRECTORY_SEPARATOR . ($file_name ?: $this->getFileName());

        return file_put_contents($path, $this->buildContent()) !== false;
    }

    /**
     * 下载为csv文件
     *
     * @param string|null $file_name
     * @return void
     */
    public function download(?string $file_name = null): void
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        // 头信息设置
        header("Content-type:text/csv");
        header("Content-Disposition:attachment;filename=" . ($file_name ?: $this->getFileName()));
        header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
        header('Expires:0');
        header('Pragma:public');
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));// 转码 防止乱码
        fputs($fp, $this->buildContent());
        ob_flush();
        flush();
        ob_end_clean();
    }

    /**
     * 生成随机字符串
     *
     * @param int $len
     * @return string
     */
    private function randomStr(int $len = 8): string
    {
        $strings = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $strings_len = strlen($strings) - 1;
        $str = '';
        while (strlen($str) < $len) {
            $str .= $strings[rand(0, $strings_len)];
        }
        return $str;
    }
}
