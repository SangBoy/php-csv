<?php

namespace SangBoy\PhpCsv;

trait CsvConfig
{

    /**
     * 分隔符
     *
     * @var string $separator
     */
    protected $separator =  ',';

    /**
     * 数值包裹符号
     *
     * @var string $enclosure
     */
    protected $enclosure =  '"';

    /**
     * @return string
     */
    public function getSeparator(): string
    {
        return $this->separator;
    }

    /**
     * @param string $separator
     * @return CsvConfig|Import
     */
    public function setSeparator(string $separator): self
    {
        $this->separator = $separator;
        return $this;
    }

    /**
     * @return string
     */
    public function getEnclosure(): string
    {
        return $this->enclosure;
    }

    /**
     * @param string|null $enclosure
     * @return CsvConfig|Import
     */
    public function setEnclosure(?string $enclosure): self
    {
        $this->enclosure = $enclosure;
        return $this;
    }
}
