# php-csv

#### 介绍
用于处理PHP中CSV数据导入和导出的操作！解决了不同语言不同编码导致的csv分割错误的问题。


#### 安装教程

`composer require sang-boy/php-csv`

#### 使用说明

1. 导入数据：

```
$csv = Csv::import($request->file('csv')->getRealPath());
try {
    $res = $csv->execute(function ($data) {
        DB::insert(...);
    });
} catch (StreamEmptyException $e) {
    return '';
}
```

2. 导出数据

```
Csv::export(['名称','性别'],[['张三','男']])->save(storage_path('csvs'));//导出至服务器
Csv::export(['名称','性别'],[['张三','男']])->download();//导出至浏览器
```
